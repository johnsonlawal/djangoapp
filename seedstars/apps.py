from django.apps import AppConfig


class SeedstarsConfig(AppConfig):
    name = 'seedstars'
