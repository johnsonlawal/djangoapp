from django import forms

from .models import Nameaddress

class NameaddressForm(forms.ModelForm):

    class Meta:
        model = Nameaddress
        fields = ('name', 'email',)
    def clean_email(self):
        email = self.cleaned_data.get('email')
        if Nameaddress.objects.filter(email=email):
            raise forms.ValidationError(("This email address is already in use. Please supply a different email address."))
        return email
