# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-13 01:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seedstars', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='nameaddress',
            name='author',
        ),
        migrations.RemoveField(
            model_name='nameaddress',
            name='created_date',
        ),
    ]
