from django.db import models
from django.utils import timezone


class Nameaddress(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField(max_length=20, default='')

    def publish(self):
        self.save()

    def __str__(self):
        return self.name