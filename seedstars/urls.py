from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^list$', views.nameaddress_list, name='nameaddress_list'),	
    url(r'^add$', views.nameaddress_add, name='nameaddress_add'),	
    url(r'^detail/(?P<pk>\d+)/$', views.nameaddress_detail, name='nameaddress_detail'),
]