from django.shortcuts import render
from django.utils import timezone
from .models import Nameaddress
from .forms import NameaddressForm
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404


def home(request):
    return render(request, 'seedstars/home.html', {})

def nameaddress_list(request):
    nameaddresses = Nameaddress.objects.order_by('-pk')
    return render(request, 'seedstars/nameaddress_list.html', {'nameaddresses': nameaddresses})
	
def nameaddress_add(request):
    if request.method == "POST":
        form = NameaddressForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('nameaddress_detail', pk=post.pk)
    else:
        form = NameaddressForm()
    return render(request, 'seedstars/nameaddress_add.html', {'form': form})
def nameaddress_detail(request, pk):
    nameaddress = get_object_or_404(Nameaddress, pk=pk)
    return render(request, 'seedstars/nameaddress_detail.html', {'nameaddress': nameaddress})